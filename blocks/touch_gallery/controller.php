<?php                  defined('C5_EXECUTE') or die(_("Access Denied."));

class TouchGalleryBlockController extends BlockController {

	protected $btTable = 'btTouchGallery';

	public function getBlockTypeName() {
		return t('Touch Gallery');
	}

	public function getBlockTypeDescription() {
		return t('A fast-loading, fully responsive, touch-enabled image gallery that looks awesome on desktop and mobile.');
	}

	private $defaultLargeWidth = 0;
	private $defaultLargeHeight = 0;
	private $defaultCropLarge = false;
	private $defaultThumbWidth = 0;
	private $defaultThumbHeight = 0;
	private $defaultMaxThumbWidth = 0;
	private $defaultMaxThumbHeight = 0;
	private $defaultCropThumb = true;
	private $defaultRandomize = false;
	private $defaultShowDesc = true;
	private $defaultShowTitle = true;
	private $defaultShowClose = true;
	private $defaultPicClick = "close";
	private $defaultHideControls = true;
	private $defaultHideDelay = 3;

	private $showLargeControls = true;
	private $showThumbControls = true;

	//Caching is disabled while in development,
	// but you should change these to TRUE for production.
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

/* **************************************************************************************************/

	protected $btInterfaceWidth = "650";
	protected $btInterfaceHeight = "700";

	public function getJavaScriptStrings() {
		return array(
			'fileset-required' => t('You must choose a file set.'),
		);
	}

	public function add() {
		$this->set('fsID', 0);
		$this->setFileSets();
		$this->setInterfaceSettings();

		//Default values for new blocks...
		$this->randomize = $this->defaultRandomize;
		$this->largeWidth = $this->defaultLargeWidth;
		$this->largeHeight = $this->defaultLargeHeight;
		$this->cropLarge = $this->defaultCropLarge;
		$this->thumbWidth = $this->defaultThumbWidth;
		$this->thumbHeight = $this->defaultThumbHeight;
		$this->cropThumb = $this->defaultCropThumb;
		$this->showDesc = $this->defaultShowDesc;
		$this->showTitle = $this->defaultShowTitle;
		$this->showClose = $this->defaultShowClose;
		$this->picClick = $this->defaultPicClick;
		$this->hideControls = $this->defaultHideControls;
		$this->hideDelay = $this->defaultHideDelay;

		$this->setNormalizedValues();
	}

	public function edit() {
		$this->setFileSets();
		$this->setInterfaceSettings();
		$this->setNormalizedValues();

	}

	private function setNormalizedValues() {
		//Don't show 0 for empty widths/heights...
		$this->set('largeWidth', empty($this->largeWidth) ? '' : $this->largeWidth);
		$this->set('largeHeight', empty($this->largeHeight) ? '' : $this->largeHeight);
		$this->set('thumbWidth', empty($this->thumbWidth) ? '' : $this->thumbWidth);
		$this->set('thumbHeight', empty($this->thumbHeight) ? '' : $this->thumbHeight);

		$this->set('cropLarge', (empty($this->largeWidth) && empty($this->largeHeight)) ? '-1' : ($this->cropLarge ? 1 : 0));
		$this->set('cropThumb', $this->cropThumb ? 1 : 0);
		$this->set('showDesc', $this->showDesc ? 1 : 0);
		$this->set('showTitle', $this->showTitle ? 1 : 0);
		$this->set('showClose', $this->showClose ? 1 : 0);
		$this->set('picClick', $this->picClick);
		$this->set('hideControls', $this->hideControls ? 1 : 0);
		$this->set('hideDelay', empty($this->hideDelay) ? '' : $this->hideDelay);
	}

	private function setInterfaceSettings() {
		$filesetsToolsURL = Loader::helper('concrete/urls')->getBlockTypeToolsURL(BlockType::getByHandle($this->btHandle)) . '/fileset_select_options';
		$this->set('filesetsToolURL', $filesetsToolsURL);
		$this->set('showLargeControls', $this->showLargeControls);
		$this->set('showThumbControls', $this->showThumbControls);
	}

	//Internal helper function (this isn't extending a block_type_controller method)
	private function getPkgHandle() {
		return BlockType::getByHandle($this->btHandle)->getPackageHandle();
	}

	private function setFileSets() {
		Loader::model('file_set');
		$fileSets = FileSet::getMySets();
		$this->set('fileSets', $fileSets);
	}

	public function save($data) {
		$data['largeWidth'] = intval($data['largeWidth']);
		$data['largeHeight'] = intval($data['largeHeight']);
		$data['thumbWidth'] = intval($data['thumbWidth']);
		$data['thumbHeight'] = intval($data['thumbHeight']);
		$data['showDesc'] = intval($data['showDesc']);
		$data['showTitle'] = intval($data['showTitle']);
		$data['showClose'] = intval($data['showClose']);
		$data['picClick'] = $data['picClick'];
		$data['hideControls'] = intval($data['hideControls']);
		$data['hideDelay'] = intval($data['hideDelay']);

		$data['cropLarge'] = (intval($data['cropLarge']) < 1) ? 0 : 1; //Watch out for the "-1" option

		parent::save($data);
	}

	private function updateHeaderItem() {
		if ($this->showTitle == 0) {
				$thumbBottommargin = 6;
		} else {
			$thumbBottommargin = intval($this->defaultMaxThumbHeight * 0.075 * 2) + 25;
		}

		$style = "<style>#touchgallery-thumbs" . $this->bID . " a{";
		$style .= "width:" . $this->defaultMaxThumbWidth . "px;";
		$style .= "height:" . $this->defaultMaxThumbHeight . "px;";
		$style .= "margin: 6px 6px " . $thumbBottommargin . "px;}";
		$style .= "#touchgallery-thumbs" . $this->bID . " a:after{";
		$style .= "bottom:" . -intval(($this->defaultMaxThumbHeight * 1.075) + 7) . "px;";
		$style .= "max-width:" . intval($this->defaultMaxThumbWidth - 10) . "px;";
		$style .= ($this->showTitle == 0) ? "display:none;}</style>" : "}</style>";
		$this->addHeaderItem($style);
	}
	private function updateFooterItem() {
		$html = Loader::helper('html');
		$this->addFooterItem($html->javascript('touch-gallery.min.js', 'touch_gallery'));

		$this->showDesc = empty($this->showDesc) ? 0 : 1;
		$this->showClose = empty($this->showClose) ? 0 : 1;
		$this->hideControls = empty($this->hideControls) ? 0 : 1;
		$this->hideDelay = empty($this->hideDelay) ? '' : $this->hideDelay;

		$js = "<script type='text/javascript'>";
		$js .= "$(document).ready(function() {";
		// $js .= "var BID_JS = " . $this->bID . ";";
		$js .= "$('#touchgallery-thumbs" . $this->bID . " a').touchGallery({";
		$js .= "bid: " . $this->bID . ",";
		$js .= "picClick: '" . $this->picClick . "',";
		$js .= "showCaptions: " . $this->showDesc . ",";
		$js .= "hideControls: " . $this->hideControls . ",";
		$js .= "showClose: " . $this->showClose . ",";
		$js .= "hideDelay: " . $this->hideDelay*1000;
		$js .= "}); });</script>";

		$this->addFooterItem($js);

	}


	public function on_page_view() {
		$files = $this->getFilesetImages($this->fsID, $this->randomize);
		$images = $this->processImageFiles($files, $this->largeWidth, $this->largeHeight, $this->cropLarge, $this->thumbWidth, $this->thumbHeight, $this->cropThumb);//here
		$this->set('images', $images);
		$this->updateHeaderItem();
		$this->updateFooterItem();
	}

	static function getFilesetImages($fsID, $randomize = false) {
		Loader::model('file_set');
		Loader::model('file_list');

		$fs = FileSet::getByID($fsID);
		$fl = new FileList();
		$fl->filterBySet($fs);
		$fl->filterByType(FileType::T_IMAGE);
		// This is really not needed and might create problems sometimes
		// $fl->setPermissionLevel('canRead');
		if ($randomize) {
			$fl->sortBy('RAND()', 'asc');
		} else {
			$fl->sortByFileSetDisplayOrder();
		}
		$files = $fl->get();
		return $files;
	}

	private function processImageFiles($imageFiles, $largeWidth, $largeHeight, $cropLarge, $thumbWidth, $thumbHeight, $cropThumbs) {
		$ih = Loader::helper('image');
		$nh = Loader::helper('navigation');

		$resizeLarge = ($largeWidth > 0 || $largeHeight > 0);
		$resizeLargeWidth = empty($largeWidth) ? 9999 : $largeWidth;
		$resizeLargeHeight = empty($largeHeight) ? 9999 : $largeHeight;
		$resizeThumb = ($thumbWidth > 0 || $thumbHeight > 0);
		$resizeThumbWidth = empty($thumbWidth) ? 9999 : $thumbWidth;
		$resizeThumbHeight = empty($thumbHeight) ? 9999 : $thumbHeight;

		$maxOrigWidth = 0;
		$maxOrigHeight = 0;
		$maxLargeWidth = 0;
		$maxLargeHeight = 0;
		$maxThumbWidth = 0;
		$maxThumbHeight = 0;

		$images = array();
		foreach ($imageFiles as $f) {
			$image = new StdClass;

			//Metadata...
			$image->fID = $f->fID;
			$image->titleRaw = $f->getTitle();
			$image->title = htmlspecialchars($image->titleRaw, ENT_QUOTES, APP_CHARSET);
			$image->descriptionRaw = $f->getDescription();
			$image->description = htmlspecialchars($image->descriptionRaw, ENT_QUOTES, APP_CHARSET);
			$linkToCID = $f->getAttribute('gallery_link_to_cid');
			$image->linkUrl = empty($linkToCID) ? '' : $nh->getLinkToCollection(Page::getByID($linkToCID));

			//Original Image (full size)...
			$image->orig = new StdClass;
			$image->orig->src = $f->getRelativePath();
			$size = getimagesize($f->getPath());
			$image->orig->width = $size[0];
			$image->orig->height = $size[1];
			$maxOrigWidth = ($image->orig->width > $maxOrigWidth) ? $image->orig->width : $maxOrigWidth;
			$maxOrigHeight = ($image->orig->height > $maxOrigHeight) ? $image->orig->height : $maxOrigHeight;

			//"Large" Size...
			if (!$resizeLarge) {
				$image->large = $image->orig;
			} else {
				$image->large = $ih->getThumbnail($f, $resizeLargeWidth, $resizeLargeHeight, $cropLarge);
			}
			$maxLargeWidth = ($image->large->width > $maxLargeWidth) ? $image->large->width : $maxLargeWidth;
			$maxLargeHeight = ($image->large->height > $maxLargeHeight) ? $image->large->height : $maxLargeHeight;

			//Thumbnail...
			if (!$resizeThumb) {
				$image->thumb = $image->orig;
			} else {
				$image->thumb = $ih->getThumbnail($f, $resizeThumbWidth, $resizeThumbHeight, $cropThumbs);
			}
			$maxThumbWidth = ($image->thumb->width > $maxThumbWidth) ? $image->thumb->width : $maxThumbWidth;
			$maxThumbHeight = ($image->thumb->height > $maxThumbHeight) ? $image->thumb->height : $maxThumbHeight;

			$images[] = $image;
		}

		//These may come in handy to the view...
		$this->set('maxOrigWidth', $maxOrigWidth);
		$this->set('maxOrigHeight', $maxOrigHeight);
		$this->set('maxLargeWidth', $maxLargeWidth);
		$this->set('maxLargeHeight', $maxLargeHeight);
		$this->set('maxThumbWidth', $maxThumbWidth);
		$this->set('maxThumbHeight', $maxThumbHeight);
		$this->defaultMaxThumbHeight = $maxThumbHeight;
		$this->defaultMaxThumbWidth = $maxThumbWidth;

		return $images;
	}

}
