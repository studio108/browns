<?php                  defined('C5_EXECUTE') or die(_("Access Denied."));
$btype = BlockType::getByHandle('touch_gallery');
$hurl = Loader::helper('concrete/urls');

$markUrl = $hurl->getBlockTypeAssetsURL($btype, 'images/194.png');
$tooltipFileSets = t("You can select any of your file sets. Any files you add later on to the file set will automatically be detected and loaded by the gallery");

$tooltipSizeText = t("<ul><li>Images will never be scaled up in size (i.e. an image smaller than the given size settings will not be enlarged).</li><li>If cropping, the width and height determine the exact size of the resized image.</li><li>If not cropping, the image is resized proportionally, so width and height determine the maximum possible size.</li><li>Setting a width or height to 0 means 'ignore this size in our calculations' (as opposed to 'make this invisible'):<ul><li>if cropping, setting one dimension to 0 means that only the <em>other</em> dimension will be cropped.</li><li>if not cropping, setting one dimension to 0 means that the image will be scaled down proportionally according to the <em>other</em> dimension.</li><li>if both width and height are set to 0, resizing/cropping will be disabled for that size. (Do this if you're not using a particular size of image in your template.)</li></ul></li></ul>");

$tooltipSetText = t("The display order can be the fileset order or random.<ul><li>Fileset order: The thumbnails will appear as they were ordered in the fileset. As of Concrete 5.4.1 you can order pictures in a fileset any way you like.</li><li>Random order: The thumbnails will appear in a different order every time the page is accessed. Ideal for a dynamic feel.</li></ul>");

$tooltipTitleText = t("The caption below each thumbnail as well as the tooltip that appears when hovering over the thumbnails are both taken directly from the 'Title' property each picture has in the fileset manager.<br />By default the 'Title' is set to the file name (file_name.jpg for instance) so we suggest you modify it to your liking.<br />Alternatively you could design a template and delete the whole '.touchgallery-thumbs a:after' selector in touchStyles.css. Doing so the thumbnails will appear without any caption.<br />We also suggest you check the comments at the beginning of the view.php file for a list of variables that offer added possibilities.");

$tooltipThumbCaptionText = t("The thumbnails' captions are extracted from each picture's title field in the file manager.");

$tooltipDescText = t("The pictures' captions are extracted from each picture's description field in the file manager.");

$tooltipCloseText = t("The add-on can show a closing icon (an 'X') in the top right corner when in full screen slideshow mode.");

$tooltipPicClick = t("When clicking (or tapping on touch screens) the fullscreen image itself, the behavior can be to <ul><li>either close the slideshow</li> <li>or show the next picture</li></ul><br /> Clicking outside the image will always close the slideshow.<br /><br />If you select 'Show next image' and there is a chance an image might cover the whole screen, you should choose to show a closing icon (option above) to avoid a situation where there is no way to close the slideshow.");

$tooltipHideControlsText = t("The captions and closing icons in full screen slideshow mode can be made to hide automatically after a delay of your choice.<br />This is especially useful on small screens so the captions and closing icons don't always sit on top of the image.");

$tooltipHideDelayText = t("The delay in seconds after which the captions and closing icons will hide when in full screen slideshow mode.<br />This value is only necessary if the auto-hide setting (on the left) is acctivated.");
?>
<link href="<?php      echo $hurl->getBlockTypeAssetsURL($btype, 'bootstrap.css'); ?>" rel="stylesheet">
<script type="text/javascript" src="<?php      echo $hurl->getBlockTypeAssetsURL($btype, 'bootstrap-tooltip.js'); ?>"></script>
<script type="text/javascript" src="<?php      echo $hurl->getBlockTypeAssetsURL($btype, 'bootstrap-popover.js'); ?>"></script>
<?php     
if (version_compare(APP_VERSION, '5.6.0', '>=')) {
?>
<style type="text/css">
.tg legend {margin-bottom: 10px; font-family: Georgia, Serif; color: #fff; background: #38beeb; padding-left: 5px;}
.tg legend i {margin: 6px 9px 0 0;}
.tg input {text-align: center;}
.tg-tooltip {float: left; margin-right: 10px!important;}
.tg-control {margin-left: 25px;}
.tg-set-list {display: table; width: 100%;}
.tg-controls-row {display:table-row;}
.tg-control-group {border-bottom: 1px solid #ececec; padding-bottom: 20px;}
.tg-control-label {font-weight: bold!important;}
.tg-set-list-item {width: 33%; display:table-cell; height: 20px; padding-top: 10px; padding-bottom: 10px; float: left; border-bottom: 1px dotted gray;}

</style>
<div class="tg ccm-ui">
	<legend><i class="icon-white icon-th"></i><?php      echo t('File sets'); ?></legend>

	<div class="control-group tg-control-group">
		<label class="control-label tg-control-label" for="fsID"><img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipFileSets; ?>" data-original-title="<?php      echo t('File Sets Selection')?>" /><?php      echo t("Select a file set"); ?></label>
		<div class="controls controls-row">
			<select id="fsID" class="span3" name="fsID">
				<option value="0"><?php      echo t('Loading&hellip;'); ?></option>
			</select>
			<span class="tg-control">[<a href="#" id="fileManagerLink"><?php      echo t('Open File Manager&hellip;'); ?></a>]</span>
		</div>
	</div>

	<div class="control-group tg-control-group">
		<label class="control-label tg-control-label" for="randomize">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipSetText; ?>" data-original-title="<?php      echo t('Display order')?>" /><?php      echo t("Display order"); ?>
		</label>
		<div class="controls">
			<?php      echo $form->select('randomize', array('0' => t('Fileset Order'), '1' => t('Random Order')), $randomize, array("class" => "span3")); ?>
		</div>
	</div>

	<legend><i class="icon-white icon-resize-small"></i><?php      echo t('Image Size'); ?></legend>

	<div class="control-group tg-control-group" <?php      echo $showLargeControls ? '' : 'style="display:none;"'; ?>>
		<label class="control-label tg-control-label" for="cropLarge">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipSizeText; ?>" data-original-title="<?php      echo t('Full-screen images resizing')?>" /><?php      echo t("Full-screen images"); ?>
		</label>
		<div class="controls controls-row">
			<?php      echo $form->select('cropLarge', array('-1' => t('Keep Original Size'), '0' => t('Shrink Proportionally'), '1' => t('Crop To Fit')), $cropLarge, array("class" => "span3")); ?>


			<span class="tg-control">
				<?php      echo t("Width "); ?><?php      echo $form->text('largeWidth', $largeWidth, array("class"=>"span1")).t(' px'); ?>
			</span>

			<span class="tg-control">
				<?php      echo t("Height "); ?><?php      echo $form->text('largeHeight', $largeHeight, array("class"=>"span1")).t(' px'); ?>
			</span>
		</div>

	</div>

	<div class="control-group tg-control-group" <?php      echo $showThumbControls ? '' : 'style="display:none;"'; ?>>
		<label class="control-label tg-control-label" for="cropThumb">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipSizeText; ?>" data-original-title="<?php      echo t('Thumbnails resizing')?>" /><?php      echo t("Thumbnails"); ?>
		</label>
		<div class="controls controls-row">
			<?php      echo $form->select('cropThumb', array('-1' => t('Keep Original Size'), '0' => t('Shrink Proportionally'), '1' => t('Crop To Fit')), $cropThumb, array("class" => "span3")); ?>

			<span class="tg-control">
				<?php      echo t("Width "); ?><?php      echo $form->text('thumbWidth', $thumbWidth, array("class"=>"span1")).t(' px'); ?>
			</span>

			<span class="tg-control">
				<?php      echo t("Height "); ?><?php      echo $form->text('thumbHeight', $thumbHeight, array("class"=>"span1")).t(' px'); ?>
			</span>
		</div>
	</div>

	<legend><i class="icon-white icon-edit"></i><?php      echo t('Captions'); ?></legend>

	<div class="control-group tg-control-group">
		<label class="control-label tg-control-label" for="showTitle">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipThumbCaptionText; ?>" data-original-title="<?php      echo t("Thumbnails captions.")?>" /><?php      echo t("Show captions under thumbnails"); ?>
		</label>
		<div class="controls">
			<label class="checkbox">
				<?php      echo $form->checkbox('showTitle', 1, intval($showTitle) == 1) ?><?php      echo t("Yes"); ?>
			</label>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label tg-control-label" for="showDesc">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipDescText; ?>" data-original-title="<?php      echo t("Fullscreen picture's caption.")?>" /><?php      echo t("Show captions under full-screen images"); ?>
		</label>
		<div class="controls">
			<label class="checkbox">
				<?php      echo $form->checkbox('showDesc', 1, intval($showDesc) == 1) ?><?php      echo t("Yes"); ?>
			</label>
		</div>
	</div>

	<legend><i class="icon-white icon-fullscreen"></i><?php      echo t('Full-screen Options'); ?></legend>

	<div class="control-group tg-control-group">
		<label class="control-label tg-control-label" for="showClose">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipCloseText; ?>" data-original-title="<?php      echo t("Show a closing icon.")?>" /><?php      echo t("Show a closing icon when in full-screen (top right corner)"); ?>
		</label>
		<div class="controls">
			<label class="checkbox">
				<?php      echo $form->checkbox('showClose', 1, intval($showClose) == 1) ?><?php      echo t("Yes"); ?>
			</label>
		</div>
	</div>

	<div class="control-group tg-control-group">
		<label class="control-label tg-control-label" for="cropThumb">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipHideControlsText; ?>" data-original-title="<?php      echo t("Auto-hide captions & controls.")?>" /><?php      echo t("Auto-hide full-screen captions and closing button"); ?>
		</label>
		<div class="controls controls-row">
			<?php      echo $form->checkbox('hideControls', 1, intval($hideControls) == 1) ?>

			<span class="tg-control">
				<?php      echo t("After "); ?><?php      echo $form->text('hideDelay', $hideDelay, array("class"=>"span1")).t(' seconds'); ?>
			</span>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label tg-control-label" for="picClick">
			<img src="<?php      echo $markUrl; ?>" width=16 height=16 class="tg-tooltip" rel="tooltip" data-content="<?php      echo $tooltipPicClick; ?>" data-original-title="<?php      echo t("Behavior when clicking on fullscreen images.")?>" /><?php      echo t("Clicking full-screen pictures will"); ?>
		</label>
		<div class="controls">
			<?php      echo $form->select('picClick', array('close' => t('Close the slideshow'), 'next' => t('Move to the next image')), $picClick, array("class" => "span3")); ?>
		</div>
	</div>
<?php      echo t('<em style="float:right; margin-top: 20px;">Back end by <strong><a href="http://www.concrete5.org/profile/-/9756/" target=_BLANK>Jordanlev</a></strong>, a 1000 thanks to him.</em>'); ?>
</div>
<?php     
} else {
?>
	<style type="text/css">
	div.ccm-pane-controls label {
		font-weight: normal !important;
		margin-bottom: 0;
		display: inline;
	}
	.gallery-display-table {
		width:100%;
		padding-bottom: 10px;
		margin-bottom:0;
	}
	.gallery-display-table td {
		padding: 10px 0;
		border-bottom: 1px dotted gray;
		vertical-align: middle;
	}
	.gallery-display-table td.label {
		text-align: right;
		padding-right: 3px;
	}
	.gallery-display-table td.label img {
	float:left;
	}
	.gallery-display-table tr.tip td {
		text-align:right;
		border-bottom:none;
	}
	.gallery-display-table tr.tip {
		height:10px;

		border-bottom:none;
	}
	.gallery-display-table input {
		text-align: center;
		width: 30px;
	}
</style>

<table border="0" cellpadding="0" cellspacing="0" class="gallery-display-table">
	<tr><td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipTitleText; ?>" data-original-title="<?php               echo t('Thumbnail\'s captions')?>" />
		<?php              echo $form->label('fsID', t('File Set:')); ?>
	</td><td colspan="5">
		<select id="fsID" name="fsID">
			<option value="0"><?php              echo t('Loading&hellip;'); ?></option>
		</select>
		&nbsp;&nbsp;&nbsp;
		[<a href="#" id="fileManagerLink"><?php              echo t('Open File Manager&hellip;'); ?></a>]
	</td></tr>

	<tr><td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipSetText; ?>" data-original-title="<?php               echo t('Display order')?>" />
		<?php              echo $form->label('randomize', t('Display Order:')); ?>
	</td><td colspan="5">
		<?php              echo $form->select('randomize', array('0' => t('Fileset Order'), '1' => t('Random Order')), $randomize); ?>
	</td></tr>

	<tr <?php              echo $showLargeControls ? '' : 'style="display:none;"'; ?>>
		<td class="label">
			<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipSizeText; ?>" data-original-title="<?php               echo t('Size options')?>" />
			<?php              echo $form->label('cropLarge', t('Size Options:')); ?>
		</td><td>
			<?php              echo $form->select('cropLarge', array('-1' => t('Keep Original Size'), '0' => t('Shrink Proportionally'), '1' => t('Crop To Fit')), $cropLarge); ?>
		</td><td class="label">
			<?php              echo $form->label('largeWidth', t('Width:')); ?>
		</td><td>
			<?php              echo $form->text('largeWidth', $largeWidth).t(' px'); ?>
		</td><td class="label">
			<?php              echo $form->label('largeHeight', t('Height:')); ?>
		</td><td>
			<?php              echo $form->text('largeHeight', $largeHeight).t(' px'); ?>
		</td>
	</tr>

	<tr <?php              echo $showThumbControls ? '' : 'style="display:none;"'; ?>>
		<td class="label">
			<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipSizeText; ?>" data-original-title="<?php               echo t('Size options')?>" />
			<?php              echo $form->label('cropThumb', t('Thumbnail Options:')); ?>
		</td><td>
			<?php              echo $form->select('cropThumb', array('0' => t('Shrink Proportionally'), '1' => t('Crop To Fit')), $cropThumb); ?>
		</td><td class="label">
			<?php              echo $form->label('thumbWidth', t('Width:')); ?>
		</td><td>
			<?php              echo $form->text('thumbWidth', $thumbWidth).t(' px'); ?>
		</td><td class="label">
			<?php              echo $form->label('thumbHeight', t('Height:')); ?>
		</td><td>
			<?php              echo $form->text('thumbHeight', $thumbHeight).t(' px'); ?>
		</td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="gallery-display-table">
	<tr>
		<td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipThumbCaptionText; ?>" data-original-title="<?php               echo t("Thumbnails' caption.")?>" />
			<?php              echo $form->label('showTitle', t('Show captions under the thumbnails?')); ?></td>
		<td><?php              echo $form->checkbox('showTitle', 1, intval($showTitle) == 1) ?></td>
		<td class="label" colspan="3">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png" width=16 height=16 class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipDescText; ?>" data-original-title="<?php               echo t("Fullscreen picture's caption.")?>" /><?php              echo $form->label('showDesc', t('Show captions with fullscreen pictures?')); ?></td>
		<td><?php              echo $form->checkbox('showDesc', 1, intval($showDesc) == 1) ?></td>
	</tr>

	<tr>
		<td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipCloseText; ?>" data-original-title="<?php               echo t("Show a closing icon.")?>" />
			<?php              echo $form->label('showClose', t('Show a closing icon (top right corner)?')); ?></td>
		<td colspan="5"><?php              echo $form->checkbox('showClose', 1, intval($showClose) == 1) ?></td>
	</tr>

	<tr>
		<td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipHideControlsText; ?>" data-original-title="<?php               echo t("Auto-hide controls.")?>" />
			<?php              echo $form->label('hideControls', t('Auto-hide full screen caption and closing button?')); ?>
		</td>
		<td><?php              echo $form->checkbox('hideControls', 1, intval($hideControls) == 1) ?></td>
		<td class="label" colspan="4" style="text-align:left;">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png" width=16 height=16 class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipHideDelayText; ?>" data-original-title="<?php               echo t("Delay before auto-hide.")?>" style="margin: 4px 20px 0 0;" /><?php              echo $form->label('hideDelay', t('After ')); ?>
			<?php              echo $form->text('hideDelay', $hideDelay).t(' seconds'); ?>
		</td>
	</tr>

	<tr>
		<td class="label">
		<img src="<?php               echo $bAssetUrl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php               echo $tooltipPicClick; ?>" data-original-title="<?php               echo t("Behavior when clicking on fullscreen images.")?>" />
			<?php              echo $form->label('picClick', t('Clicking fullscreen pictures will:')); ?>
		</td>
		<td colspan="5"><?php              echo $form->select('picClick', array('close' => t('Close the slideshow'), 'next' => t('Show next image')), $picClick); ?></td>
	</tr>

</table>
<?php              echo t('<em style="float:right; margin-top: 20px;">Back end by <strong><a href="http://www.concrete5.org/profile/-/9756/" target=_BLANK>Jordanlev</a></strong>, a 1000 thanks to him.</em>');
}
?>

<script type="text/javascript">
	var FILESETS_URL = '<?php              echo $filesetsToolURL ?>';
	refreshFilesetList(<?php              echo $fsID ?>);
	$('.tg-tooltip').hover(function()
		{

			$(this).popover('show')

		});
</script>
