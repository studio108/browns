<?php                  defined('C5_EXECUTE') or die(_("Access Denied."));
global $c;
if ($c->isEditMode() == TRUE) {
if ($showTitle == 0) {
      $thumbBottommargin = 6;
    } else {
      $thumbBottommargin = intval($maxThumbHeight * 0.075 * 2) + 25;
    }

?>
<style>
#touchgallery-thumbs<?php               echo $bID ?> a{
  width:<?php               echo $maxThumbWidth ?>px;
  height:<?php               echo $maxThumbHeight ?>px;
  margin: 6px 6px <?php             echo $thumbBottommargin ?>px;
}
#touchgallery-thumbs<?php               echo $bID ?> a:after{
    bottom: -<?php               echo intval(($maxThumbHeight * 1.075) + 7) ?>px; 
    max-width: <?php               echo intval($maxThumbWidth - 10) ?>px;
    <?php               if ($showTitle == 0) {echo 'display:none;';} ?>
}
</style>
<?php             } ?>
<div class="touchgallery-thumbs" id="touchgallery-thumbs<?php              echo $bID ?>">

	<?php                  foreach ($images as $img): ?>
		<a href="<?php              echo $img->large->src ?>" style="background-image:url(<?php              echo $img->thumb->src ?>);" title="<?php              echo $img->title ?>"<?php              if ($img->description && $showDesc) { echo ' data-tg-desc="' . $img->description . '"';} ?>></a>
	<?php                  endforeach; ?>

</div>