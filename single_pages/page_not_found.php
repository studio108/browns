<?php defined('C5_EXECUTE') or die("Access Denied."); ?>




<!-- Page titles -->
<div class="row">
<div class="large-12 columns heading"> <h1 class="error"><?php echo t('Page Not Found')?></h1></div>
</div>


<!-- Main Content -->
<div class="row">
<div class="large-8 large-offset-2 columns content centered-text"> <?php echo t('Sorry, no page could be found at this address.')?>

<?php if (is_object($c)) { ?>
	<br/><br/>
	<?php $a = new Area("Main"); $a->display($c); ?>
<?php } ?>

<br/><br/>

<a class="button secondary" href="<?php echo DIR_REL?>/"><?php echo t('Back to Home')?></a> </div>
</div>
    