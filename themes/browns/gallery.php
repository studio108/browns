<?php defined('C5_EXECUTE') or die('Access Denied.') ?>
<?php $this->inc('elements/header.php');?>

<!-- Page titles -->
<div class="row">
<div class="large-12 columns heading gallery"> 
<?php echo '<h1>' . $c->getCollectionName() . '</h1>' ?></div>
</div>

<!-- Main Content -->
<div class="row">
<div class="small-12 small-centered columns content"> <?php $a = new Area('main'); $a->display($c);?> </div>
</div>



<?php
$this->inc('elements/footer.php');
?>
