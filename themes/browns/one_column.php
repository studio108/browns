<?php defined('C5_EXECUTE') or die('Access Denied.') ?>
<?php $this->inc('elements/header.php');?>

<!-- Page titles -->
<div class="row">
<div class="large-12 columns heading"> <?php echo '<h1>' . $c->getCollectionName() . '</h1>' ?></div>
</div>


<!-- Main Content -->
<div class="row">
<div class="large-8 large-offset-2 columns content"> <?php $a = new Area('main'); $a->display($c);?> </div>
</div>



<?php
$this->inc('elements/footer.php');
?>