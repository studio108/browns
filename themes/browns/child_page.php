<?php defined('C5_EXECUTE') or die('Access Denied.') ?>
<?php $this->inc('elements/header.php');?>

<!-- Page titles -->
<div class="row">
<div class="large-12 columns heading"> <?php  $page=Page::getByID($c->getCollectionParentID());  echo '<h1>' . $page->getCollectionName() .'</h1>'?></div>
</div>

<!-- Header image -->
<div class="row">
<div class="large-12 columns header_image"> <?php $a = new Area('header_image'); $a->display($c);?> </div>
</div>

<!-- Main Content -->
<div class="row">
<div class="large-4 columns content"> <?php $a = new Area('left_col'); $a->display($c);?> </div>
<div class="large-8 columns content"> 
<?php echo '<h2>' . $c->getCollectionName() . '</h2>' ?>
<?php $a = new Area('main'); $a->display($c);?> </div>
</div>



<?php
$this->inc('elements/footer.php');
?>