<?php defined('C5_EXECUTE') or die('Access Denied.') ?>
<?php $this->inc('elements/header.php');?>

<div class="row">
<div class="large-12 columns heading blog"> <?php  $page=Page::getByID($c->getCollectionParentID());  echo '<h1>' . $page->getCollectionName() .'</h1>'?>
<?php echo '<h2>' . $c->getCollectionName() . '</h2>' ?></div>
</div>


<!-- Main Content -->
<div class="row">
<div class="large-8 large-offset-2 columns content"> 
<?php $a = new Area('Thumbnail Image');$a->display($c);?>
<?php $a = new Area('main'); $a->display($c);?> 

</div>
</div>



<?php
$this->inc('elements/footer.php');
?>