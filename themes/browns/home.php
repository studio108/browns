<?php defined('C5_EXECUTE') or die('Access Denied.') ?>
<?php $this->inc('elements/header.php');?>

<!-- Home Slider -->
<div class="row slider_wrapper">
<div class="large-12 columns slider"> <?php $a = new Area('home_slider'); $a->display($c);?> </div>
</div>

<div class="row">
<div class="large-11 large-centered  small-11 small-centered columns"> <?php $a = new Area('col_2'); $a->display($c);?> </div>
</div>


<?php
$this->inc('elements/footer.php');
?>