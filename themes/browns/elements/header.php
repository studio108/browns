<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>

<?php Loader::element('header_required'); ?>


<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="<?php echo $this->getThemePath(); ?>/css/foundation.min.css" rel="stylesheet">
<link href="<?php echo $this->getThemePath(); ?>/css/main.min.css" rel="stylesheet">
<link href="<?php echo $this->getThemePath(); ?>/css/animate.css" rel="stylesheet">

<link href="<?php echo $this->getThemePath(); ?>/typography.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
  
<script src="<?php echo $this->getThemePath(); ?>/js/vendor/custom.modernizr.js"></script>

</head>
<body>
<!-- Main Container OPEN -->
<div class="container animated fadeInUp">

<!-- Header and Nav OPEN -->
<!-- FIXED OPEN --><div class="header_area ">
<div class="row">
<div class="large-12 columns logo "> <a href="/"><img src="<?php echo $this->getThemePath()?>/images/browns-woodworking-logo@2x.gif" width="180" height="130" alt="Browns Woodworking Logo "></a></div>
</div>


<div class="row">
<div class="large-12 columns nav "> 
<div class="contain-to-grid ">


<nav class="top-bar">
  <ul class="title-area">
    <!-- Title Area -->
    <li class="name"></li>
    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Left Nav Section -->
    
<?php
$nav = BlockType::getByHandle('autonav');
$nav->controller->orderBy = 'display_asc';
$nav->controller->displayPages = 'top';
$nav->controller->displaySubPages = 'all';
$nav->controller->displaySubPageLevels = 'all';
$nav->render('templates/foundation_topnav');
?>

  </section>
</nav>
  
  </div>
    
  </div>
</div>
<!-- FIXED CLOSE --> </div>



<!-- End Header and Nav CLOSE -->