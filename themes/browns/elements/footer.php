<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

<!-- Footer OPEN -->
<div class="footer">
<div class="large-8 large-centered small-12 small-centered columns"> <?php $a = new GlobalArea('footer_col-1');$a->display($c);?></div>
<!-- Footer CLOSE --> </div>

<div class= "row">
<div class= "large-12 columns copyright">
 <p> &copy; <?php echo date("Y") ?> Browns Woodworking&nbsp;&nbsp;| &nbsp;&nbsp;<a href="../contact-us">Contact</a> &nbsp;&nbsp;| &nbsp;&nbsp;<a href="/sitemap">Sitemap</a>&nbsp;&nbsp;| &nbsp;&nbsp;<a href="/cookie-policy">Cookie Policy</a> &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.studio108.co.uk" target="_blank">Design: Studio 108</a></p>
</div>
 </div>


<!-- Main Container CLOSE -->
</div>

<a href="#0" class="cd-top">Top</a>


  <?php Loader::element('footer_required'); ?>
  
  <script src="<?php echo $this->getThemePath(); ?>/js/foundation.min.js" type="text/javascript"></script>

<script src="<?php echo $this->getThemePath(); ?>/js/scroll_back.js" type="text/javascript"></script>



  <!--
  
  <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.interchange.js"></script>
  
  <script src="js/foundation/foundation.abide.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>
  
  -->
  
  <script>
    $(document).foundation();
  </script>
  

</body>
</html>

